"use strict";

// масив книг
const books = [
  {
    author: "Люсі Фолі",
    name: "Список запрошених",
    price: 70,
  },
  {
    author: "Сюзанна Кларк",
    name: "Джонатан Стрейндж і м-р Норрелл",
  },
  {
    name: "Дизайн. Книга для недизайнерів.",
    price: 70,
  },
  {
    author: "Алан Мур",
    name: "Неономікон",
    price: 70,
  },
  {
    author: "Террі Пратчетт",
    name: "Рухомі картинки",
    price: 40,
  },
  {
    author: "Анґус Гайленд",
    name: "Коти в мистецтві",
  },
];

// перевіряємо книгу на наявність всіх трьох властивостей
function validateBook(book) {
  if (!book.author) {
    throw new Error(`Помилка! Книга '${book.name}' не має автора`);
  }
  if (!book.name) {
    throw new Error(`Помилка! Книга від автора '${book.author}' не має назви`);
  }
  if (!book.price) {
    throw new Error(`Помилка! Книга '${book.name}' не має вказаної ціни`);
  }
}

// створюємо список
function renderBooks(books) {
  const root = document.getElementById("root");
  const list = document.createElement("ul");

  books.forEach((book) => {
    try {
      validateBook(book);

      const li = document.createElement("li");
      li.textContent = `Автор: ${book.author} - ${book.name} - ${book.price} грн`;

      list.appendChild(li);
    } catch (error) {
      console.error(error.message);
    }
  });

  root.appendChild(list);
}

renderBooks(books);
